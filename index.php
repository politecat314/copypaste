<?php
include 'db_connection.php';

function getTitle($url) {
    return file_get_contents('http://textance.herokuapp.com/title/'.$url);
}

function sendToDb($title, $url) {
    $conn = OpenCon();

    $sql = "INSERT INTO links (title, url)
            VALUES ('$title', '$url')";

    if (!$conn->query($sql) === TRUE) { // echo error message if failed to save in database
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    CloseCon($conn);
    
}

function removeFromDb($pk) {
    $conn = OpenCon();

    $sql = "DELETE FROM links 
            WHERE pk = $pk";

    if (!$conn->query($sql) === TRUE) { // echo error message if failed to save in database
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    CloseCon($conn);
    
}



if (isset($_POST['url'])) {
    $url = $_POST['url'];
    $title = getTitle($url);

    sendToDb($title, $url);
} else if (isset($_POST['pk'])) {
    removeFromDb($_POST['pk']);
}





?>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link rel="stylesheet" href="style.css">

    <!-- <link rel="icon" type="image/svg" href="/components/logo.svg"/> -->
    <title>Copypaste</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="#">Copypaste</a>
            <!-- <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button> -->
            <!-- <div class="collapse navbar-collapse" id="navbarText">
                <ul id="link_list" class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Pricing</a>
                    </li>
                </ul>
                <span class="navbar-text">
                    Navbar text with an inline element
                </span>
            </div> -->
        </div>
    </nav>


    <div class="container">
        <br>
        

        <div class="row">
            <form class="align-items-center" method="post" onsubmit="return validateForm()">
                <div class="col-12">
                    <label class="visually-hidden" for="inlineFormInputinlineFormInputUrl">Url</label>
                    <div class="input-group">
                        <div class="input-group-text">🗎</div>
                        <input name="url" type="text" class="form-control" id="inlineFormInputUrl" placeholder="Paste your link here">
                        <button id="submitButton" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>


            </form>
        </div>

        <hr>

        <div class="row">
            <div class="col">
                <div class="list-group">
                    <!-- <a href="#" class="list-group-item list-group-item-action active" aria-current="true">
                        The current link item
                    </a> -->
                    <form method="post">
                    <!-- <a href="#" class="list-group-item list-group-item-action">A second link item<button class="close" name="thisbutton" type="submit"><i class="fas fa-times-circle"></i></button></a> -->
                    <!-- <a href="#" class="list-group-item list-group-item-action">A second link item<span class="close"><i class="fas fa-times-circle"></i></span></a>
                    <a href="#" class="list-group-item list-group-item-action">A second link item<button class="close" name="thisbutton" type="submit"><i class="fas fa-times-circle"></i></button></a> -->
                    <!-- <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1" aria-disabled="true">A disabled link item</a> -->
                <?php 
                $conn = OpenCon();

                $sql = "SELECT * FROM links";
                $result = $conn->query($sql);
                
                if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                      echo '<a href="'.$row['url'].'" class="list-group-item list-group-item-action">'.$row['title'].'<button class="close" name="pk" value="'.$row["pk"].'" type="submit"><i class="fas fa-times-circle"></i></button></a>';
                        // echo '<a href="'.$row["url"].'" class="list-group-item list-group-item-action">'.$row['title'].'<span class="close"><i class="fas fa-times-circle"></i></span></a><button class="close" name="'.$row["pk"].'" type="submit"><i class="fas fa-times-circle"></i></button>';
                    }
                  } else {
                    echo "No urls in the database";
                  }
                
                
                CloseCon($conn);
                ?>

                </form>
                
                </div>
            </div>
        </div>


    </div>




    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    

    <script src="script.js"></script>
</body>

</html>